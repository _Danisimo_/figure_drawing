package Task;

public class Triangle {

    public static void main (String args[]){
    rightTriangle1();
    rightTriangle2();
    rightTriangle3();
    rightTriangle4();

    triangle1();
    triangle2();
    triangle3();
    triangle4();
    }

    /*
        *
        **
        ***
        ****
        *****

     */
    public static void rightTriangle1()
    {
        for (int x = 1; x <= 5; x++) {
            for (int i = 1; i <= x; i++) {
                System.out.print(" *");
            }
            System.out.println(" ");
        }

        System.out.println("-----------------------------");
    }

    public static void rightTriangle2()
    {
        for(int y=1;y<=5;y++){
            for(int k=5;k>y;k--){
                System.out.print(" ");
            }
            for(int x=1;x<=y;x++){
                System.out.print("*");
            }
            System.out.println();
        }
        System.out.println("-----------------------------");
    }
    public static void rightTriangle3()
    {
        for(int i = 0; i<5;i++){
            for(int j = 5; i<j;j--){
                System.out.print(" *");
            }
            System.out.println();
        }

        System.out.println("-----------------------------");
    }
    public static void rightTriangle4()
    {
        for (int i = 0; i <= 5; i++) {
            for (int j = 0; j < i; j++) {

                System.out.print("  ");
            }
            for (int j = 5; j > i; j--) {

                System.out.print(" *");
            }

            System.out.println();
        }


        System.out.println("-----------------------------");
    }

    public static void triangle1(){
        for (int i = 5; i >= 0; i--){

            for (int j = 5; j > i; j--){
                System.out.print("   ");
            }

            for (int j1 = 0; j1 <= i; j1++){
                System.out.print(" * ");
            }

            for (int j = 0; j < i; j++){
                System.out.print(" * ");
            }

            for (int j = 5; j > i; j--){
                System.out.print("   ");
            }

            System.out.println();
        }
        System.out.println("-----------------------------");
    }
    public static void triangle2(){
        for (int i = 1; i <= 5 * 2 - 1; i += 2) {

            for (int j = 0; j < (5 * 2 - 1 - i) / 2; j++){
                System.out.print("   ");
            }
            for (int j = 0; j < i; j++){
                System.out.print(" * " );
            }
            System.out.print("\n");
        }
        System.out.println("-----------------------------");
    }
    public static void triangle3(){
        for (int i = 5; i >= 0; i--) {
            for (int j = 0; j < 5; j++) {
                if (j < i) {
                    System.out.print("   ");
                } else
                    System.out.print(" * ");
            }
            System.out.println();
        }
        for (int i = 1; i <= 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (j < i) {
                    System.out.print("   ");
                } else
                    System.out.print(" * ");
            }
            System.out.println();
        }

        System.out.println("-----------------------------");
    }

    public static void triangle4(){
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j <= i; j++) {

                System.out.print(" * ");
            }
            System.out.println();
        }
        for (int i = 1; i <= 5; i++) {
            for (int j = 5; j > i; j--) {
                System.out.print(" * ");
            }
            System.out.println();
        }
        System.out.println("-----------------------------");
    }


}
